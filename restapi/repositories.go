package restapi

import (
	"gitlab.com/aleksandrzaykov88/softweather/internal/database"
	"gitlab.com/aleksandrzaykov88/softweather/internal/repositories"
	"gitlab.com/aleksandrzaykov88/softweather/internal/repositories/tasks"
	"gitlab.com/aleksandrzaykov88/softweather/internal/repositories/transactions"
	"gitlab.com/aleksandrzaykov88/softweather/internal/repositories/users"
)

// Repositories describes a repositories list.
type Repositories struct {
	Users        repositories.Users
	Tasks        repositories.Tasks
	Transactions repositories.Transactions
}

// configureRepositories open database connection and add it into repositories.
func configureRepositories() (Repositories, error) {
	db, err := database.Get()
	if err != nil {
		return Repositories{}, err
	}

	return Repositories{
		Users:        users.New(db),
		Tasks:        tasks.New(db),
		Transactions: transactions.New(db),
	}, nil
}

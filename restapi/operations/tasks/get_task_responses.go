// Code generated by go-swagger; DO NOT EDIT.

package tasks

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"io"
	"net/http"

	"github.com/go-openapi/runtime"

	"gitlab.com/aleksandrzaykov88/softweather/models"
)

// GetTaskOKCode is the HTTP code returned for type GetTaskOK
const GetTaskOKCode int = 200

/*
GetTaskOK OK

swagger:response getTaskOK
*/
type GetTaskOK struct {

	/*
	  In: Body
	*/
	Payload io.ReadCloser `json:"body,omitempty"`
}

// NewGetTaskOK creates GetTaskOK with default headers values
func NewGetTaskOK() *GetTaskOK {

	return &GetTaskOK{}
}

// WithPayload adds the payload to the get task o k response
func (o *GetTaskOK) WithPayload(payload io.ReadCloser) *GetTaskOK {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the get task o k response
func (o *GetTaskOK) SetPayload(payload io.ReadCloser) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *GetTaskOK) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(200)
	payload := o.Payload
	if err := producer.Produce(rw, payload); err != nil {
		panic(err) // let the recovery middleware deal with this
	}
}

// GetTaskBadRequestCode is the HTTP code returned for type GetTaskBadRequest
const GetTaskBadRequestCode int = 400

/*
GetTaskBadRequest Bad Request

swagger:response getTaskBadRequest
*/
type GetTaskBadRequest struct {

	/*
	  In: Body
	*/
	Payload *models.Error `json:"body,omitempty"`
}

// NewGetTaskBadRequest creates GetTaskBadRequest with default headers values
func NewGetTaskBadRequest() *GetTaskBadRequest {

	return &GetTaskBadRequest{}
}

// WithPayload adds the payload to the get task bad request response
func (o *GetTaskBadRequest) WithPayload(payload *models.Error) *GetTaskBadRequest {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the get task bad request response
func (o *GetTaskBadRequest) SetPayload(payload *models.Error) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *GetTaskBadRequest) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(400)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}

// GetTaskForbiddenCode is the HTTP code returned for type GetTaskForbidden
const GetTaskForbiddenCode int = 403

/*
GetTaskForbidden Forbidden

swagger:response getTaskForbidden
*/
type GetTaskForbidden struct {

	/*
	  In: Body
	*/
	Payload *models.Error `json:"body,omitempty"`
}

// NewGetTaskForbidden creates GetTaskForbidden with default headers values
func NewGetTaskForbidden() *GetTaskForbidden {

	return &GetTaskForbidden{}
}

// WithPayload adds the payload to the get task forbidden response
func (o *GetTaskForbidden) WithPayload(payload *models.Error) *GetTaskForbidden {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the get task forbidden response
func (o *GetTaskForbidden) SetPayload(payload *models.Error) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *GetTaskForbidden) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(403)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}

// GetTaskInternalServerErrorCode is the HTTP code returned for type GetTaskInternalServerError
const GetTaskInternalServerErrorCode int = 500

/*
GetTaskInternalServerError Internal Server Error

swagger:response getTaskInternalServerError
*/
type GetTaskInternalServerError struct {

	/*
	  In: Body
	*/
	Payload *models.Error `json:"body,omitempty"`
}

// NewGetTaskInternalServerError creates GetTaskInternalServerError with default headers values
func NewGetTaskInternalServerError() *GetTaskInternalServerError {

	return &GetTaskInternalServerError{}
}

// WithPayload adds the payload to the get task internal server error response
func (o *GetTaskInternalServerError) WithPayload(payload *models.Error) *GetTaskInternalServerError {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the get task internal server error response
func (o *GetTaskInternalServerError) SetPayload(payload *models.Error) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *GetTaskInternalServerError) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(500)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}

// This file is safe to edit. Once it exists it will not be overwritten

package restapi

import (
	"crypto/tls"
	"fmt"
	"gitlab.com/aleksandrzaykov88/softweather/pkg/utils"
	"gitlab.com/aleksandrzaykov88/softweather/restapi/operations/tasks"
	"gitlab.com/aleksandrzaykov88/softweather/restapi/operations/transactions"
	"net/http"
	"os"
	"path/filepath"

	"github.com/rs/cors"
	"gitlab.com/aleksandrzaykov88/softweather/internal/handlers"
	u "gitlab.com/aleksandrzaykov88/softweather/internal/repositories/users"
	"gitlab.com/aleksandrzaykov88/softweather/restapi/operations/users"

	"github.com/rs/zerolog/log"
	"gitlab.com/aleksandrzaykov88/softweather/internal/config"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/runtime/middleware"

	"gitlab.com/aleksandrzaykov88/softweather/restapi/operations"
)

//go:generate swagger generate server --target ../../softweather --name SoftWeatherAPI --spec ../api/swagger.yaml --principal interface{}

func configureFlags(api *operations.SoftWeatherAPIAPI) {
	// Setup env-variables.
	// From cfg by default.
	host := os.Getenv("HOST")
	port := os.Getenv("PORT")
	if port == "" || host == "" {
		cfg := config.Get()
		host = cfg.HTTP.Host
		port = cfg.HTTP.Port
	}
	if err := os.Setenv("HOST", host); err != nil {
		log.Fatal().Err(err)
	}
	if err := os.Setenv("PORT", port); err != nil {
		log.Fatal().Err(err)
	}
}

func configureAPI(api *operations.SoftWeatherAPIAPI) http.Handler {
	// configure the api here
	api.ServeError = errors.ServeError

	// Set your custom logger if needed. Default one is log.Printf
	// Expected interface func(string, ...interface{})
	//
	// Example:
	// api.Logger = log.Printf

	var (
		userID   int
		userRole string
	)
	api.XTokenAuth = func(token string) (interface{}, error) {
		var err error
		userID, userRole, err = u.IsJWTValid(token)
		if err != nil {
			return nil, err
		}

		return token, nil
	}

	api.UseSwaggerUI()
	// To continue using redoc as your UI, uncomment the following line
	// api.UseRedoc()

	api.JSONConsumer = runtime.JSONConsumer()

	api.JSONProducer = runtime.JSONProducer()

	// Initializing repositories.
	repos, err := configureRepositories()
	if err != nil {
		log.Error().Err(err)
		return nil
	}

	// TRANSACTIONS
	api.TransactionsRefillHandler = transactions.RefillHandlerFunc(func(params transactions.RefillParams, principal interface{}) middleware.Responder {
		return handlers.Refill(repos.Transactions, params, userID, userRole)
	})

	// TASKS
	api.TasksGetTaskHandler = tasks.GetTaskHandlerFunc(func(params tasks.GetTaskParams, principal interface{}) middleware.Responder {
		return middleware.ResponderFunc(func(rw http.ResponseWriter, p runtime.Producer) {
			status, taskPath, err := handlers.GetTask(repos.Tasks, params, userID, userRole)
			if err != nil {
				log.Error().Err(err).Msg("get task")
				rw.WriteHeader(status)
				rw.Write([]byte(err.Error()))
				return
			}
			taskFile, err := utils.GetFile(taskPath)
			if err != nil {
				log.Error().Err(err).Msg("get task")
				rw.WriteHeader(http.StatusInternalServerError)
				rw.Write([]byte(err.Error()))
				return
			}

			attach := fmt.Sprintf("attachment; filename=%s", filepath.Base(taskPath))
			rw.Header().Set("Content-Disposition", attach)
			rw.Header().Set("Content-Type", "application/octet-stream")
			if _, err := rw.Write(taskFile); err != nil {
				log.Error().Err(err).Msg("get task")
				rw.WriteHeader(http.StatusInternalServerError)
				return
			}
		})
	})

	// USERS
	api.UsersRegistrationHandler = users.RegistrationHandlerFunc(func(params users.RegistrationParams) middleware.Responder {
		return handlers.RegisterUser(repos.Users, params)
	})
	api.UsersGetUsersHandler = users.GetUsersHandlerFunc(func(params users.GetUsersParams, principal interface{}) middleware.Responder {
		return handlers.GetUsers(repos.Users, params)
	})
	api.UsersLoginHandler = users.LoginHandlerFunc(func(params users.LoginParams) middleware.Responder {
		return handlers.Login(repos.Users, params)
	})

	if api.UsersDeleteUserHandler == nil {
		api.UsersDeleteUserHandler = users.DeleteUserHandlerFunc(func(params users.DeleteUserParams, principal interface{}) middleware.Responder {
			return middleware.NotImplemented("operation user.DeleteUser has not yet been implemented")
		})
	}
	if api.UsersGetUserHandler == nil {
		api.UsersGetUserHandler = users.GetUserHandlerFunc(func(params users.GetUserParams, principal interface{}) middleware.Responder {
			return middleware.NotImplemented("operation user.GetUser has not yet been implemented")
		})
	}

	if api.UsersUpdateUserHandler == nil {
		api.UsersUpdateUserHandler = users.UpdateUserHandlerFunc(func(params users.UpdateUserParams, principal interface{}) middleware.Responder {
			return middleware.NotImplemented("operation user.UpdateUser has not yet been implemented")
		})
	}

	api.PreServerShutdown = func() {}

	api.ServerShutdown = func() {}

	return setupGlobalMiddleware(api.Serve(setupMiddlewares))
}

// The TLS configuration before HTTPS server starts.
func configureTLS(tlsConfig *tls.Config) {
	// Make all necessary changes to the TLS configuration here.
}

// As soon as server is initialized but not run yet, this function will be called.
// If you need to modify a config, store server instance to stop it individually later, this is the place.
// This function can be called multiple times, depending on the number of serving schemes.
// scheme value will be set accordingly: "http", "https" or "unix".
func configureServer(s *http.Server, scheme, addr string) {
}

// The middleware configuration is for the handler executors. These do not apply to the swagger.json document.
// The middleware executes after routing but before authentication, binding and validation.
func setupMiddlewares(handler http.Handler) http.Handler {
	return handler
}

// The middleware configuration happens before anything, this middleware also applies to serving the swagger.json document.
// So this is a good place to plug in a panic handling middleware, logging and metrics.
func setupGlobalMiddleware(handler http.Handler) http.Handler {
	cfg := config.Get()
	handleCORS := cors.New(cors.Options{
		AllowedOrigins:   cfg.HTTP.TrustedSources,
		AllowedMethods:   []string{"POST", "GET", "PATCH", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Content-Type"},
		AllowCredentials: true,
	})

	return handleCORS.Handler(handler)
}

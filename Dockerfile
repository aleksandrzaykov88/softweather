FROM golang

WORKDIR /app

COPY . .

EXPOSE 21210

RUN go mod tidy

RUN go build -o softweather ./cmd/soft-weather-api-server/

CMD ["./softweather"]
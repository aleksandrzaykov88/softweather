package main

import (
	"path/filepath"
	"runtime"

	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
	"gitlab.com/aleksandrzaykov88/softweather/internal/config"
	"gitlab.com/aleksandrzaykov88/softweather/internal/database"
)

func init() {
	// Init config.
	_, filename, _, _ := runtime.Caller(0)
	viper.SetConfigFile(filepath.Join(filepath.Dir(filename), "config.json"))

	// Init database.
	cfg := config.Get()
	if err := database.Create(cfg.DB.ConnectionString); err != nil {
		log.Fatal().Err(err)
	}
}

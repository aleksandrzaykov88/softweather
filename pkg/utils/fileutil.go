package utils

import "os"

// GetFile open and return file as byte slice.
func GetFile(filepath string) ([]byte, error) {
	data, err := os.ReadFile(filepath)
	if err != nil {
		return nil, err
	}
	return data, nil
}

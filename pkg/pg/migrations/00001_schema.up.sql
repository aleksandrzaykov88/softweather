CREATE OR REPLACE FUNCTION z_slug(arg_text text)
    RETURNS text as
$BODY$
DECLARE
    output_text text;
BEGIN
    output_text := translate(arg_text,
                             'абвгдежзийклмнопрстуфхцчшщъыьэюя',
                             'abvgdegziyklmnoprstufhzcss_y_eua');
    output_text := regexp_replace(output_text, '[^a-zA-Z0-9]+', '-', 'g');
    output_text := regexp_replace(output_text, '^-|-$', '', 'g');
    output_text := lower(output_text);

    RETURN output_text;
END;
$BODY$
    LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION z_exception_if(
    condition boolean,
    message text
)
    RETURNS VOID AS $$
BEGIN
    IF condition THEN
        RAISE EXCEPTION '%', message;
    END IF;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.fn_user_ins(
    arg_name text,
    arg_sname text,
    arg_pname text,
    arg_login text,
    arg_password text,
    arg_group text
)
    RETURNS TEXT AS
$$
declare
    v_id int;
    v_group_id int;
    v_student_id int;
    v_login text;
    v_temp text;
    v_counter int = 1;
begin
    perform z_exception_if(arg_name is null or arg_name = '', 'User name is empty');
    perform z_exception_if(arg_sname is null or arg_sname = '', 'User surname is empty');
    perform z_exception_if(arg_password is null or arg_password = '', 'Password is empty');

    v_login = arg_login;
    if arg_login is null or arg_login = '' then
        v_login = z_slug(arg_name || ' ' || arg_sname);
    end if;

    v_temp = v_login;
    while exists(
        SELECT 1
        FROM t_user u
        WHERE u.login = v_login
    ) loop
            v_login = v_temp || '_' || v_counter::TEXT;
            v_counter = v_counter + 1;
        end loop;

    insert into t_user as u (
        login,
        password_hash,
        name,
        sname,
        pname
    ) values (
                 v_login,
                 arg_password,
                 arg_name,
                 arg_sname,
                 arg_pname
             ) returning u.id into v_id;

    if arg_group is not null and arg_group <> '' then
        insert into t_university_group as ug (
            name
        ) values (
                     arg_group
                 ) returning ug.id into v_group_id;

        insert into t_student as s (
            user_id,
            group_id
        ) values (
                     v_id,
                     v_group_id
                 ) returning s.id into v_student_id;

        insert into t_account as a (
            student_id
        ) values (
                     v_student_id
                 );

        update t_user set
            role = 'student'
        where id = v_id;
    end if;

    return v_login;
end;
$$
    LANGUAGE plpgsql;

ALTER FUNCTION public.fn_user_ins(text, text, text, text, text, text) OWNER TO postgres;
COMMENT ON FUNCTION public.fn_user_ins(text, text, text, text, text, text) IS 'Users. Registration';

------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.fn_transaction_refill(
    arg_user_id int,
    arg_amount int,
    arg_user_role text
)
    RETURNS INT AS
$$
declare
    v_balance int;
    v_account_id int;
    v_order_id int;
    v_order_type_id int;
    v_transaction_type_id int;
    v_transaction_type text = 'positive';
    v_order_type text = 'refill';
begin
    perform z_exception_if(arg_user_id is null, 'User id is empty');
    perform z_exception_if(arg_amount is null, 'Payment amount empty');
    perform z_exception_if(arg_user_role is null or arg_user_role = '' or arg_user_role = 'user', 'Incorrect role');

    select ta.id
    from t_account ta
             join t_student ts on ta.student_id = ts.id
             join t_user tu on ts.user_id = tu.id
    where tu.id = arg_user_id
    into v_account_id;
    perform z_exception_if(v_account_id is null, 'Account id not found');

    select tot.id
    from t_order_type tot
    where tot."name" = v_order_type
    into v_order_type_id;
    perform z_exception_if(v_order_type_id is null, 'Order type not found');

    select ttt.id
    from t_transaction_type ttt
    where ttt."name" = v_transaction_type
    into v_transaction_type_id;
    perform z_exception_if(v_transaction_type_id is null, 'Transaction type not found');

    insert into t_order as o (
        type_id,
        amount
    ) values (
                 v_order_type_id,
                 arg_amount
             ) returning o.id into v_order_id;
    perform z_exception_if(v_order_id is null, 'Order not created');

    insert into t_transaction as tt (
        account_id,
        date,
        type_id,
        order_id
    ) values (
                 v_account_id,
                 CURRENT_DATE,
                 v_transaction_type_id,
                 v_order_id
             );

    update t_account ta set
        balance = balance + arg_amount
    where ta.id = v_account_id
    returning ta.balance into v_balance;

    return v_balance;
end;
$$
    LANGUAGE plpgsql;

ALTER FUNCTION public.fn_transaction_refill(int, int, text) OWNER TO postgres;
COMMENT ON FUNCTION public.fn_transaction_refill(int, int, text) IS 'Transactions. Refill';

------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.fn_transaction_task(
    arg_user_id int,
    arg_task_id int,
    arg_user_role text
)
    RETURNS INT AS
$$
declare
    v_balance int;
    v_task_price int;
    v_account_id int;
    v_order_id int;
    v_order_type_id int;
    v_transaction_type_id int;
    v_transaction_type text = 'negative';
    v_order_type text = 'task';
begin
    perform z_exception_if(arg_user_id is null, 'User id is empty');
    perform z_exception_if(arg_task_id is null, 'Task id is empty');
    perform z_exception_if(arg_user_role is null or arg_user_role = '' or arg_user_role = 'user', 'Incorrect role');

    select ta.id
    from t_account ta
             join t_student ts on ta.student_id = ts.id
             join t_user tu on ts.user_id = tu.id
    where tu.id = arg_user_id
    into v_account_id;
    perform z_exception_if(v_account_id is null, 'Account id not found');

    select tot.id
    from t_order_type tot
    where tot."name" = v_order_type
    into v_order_type_id;
    perform z_exception_if(v_order_type_id is null, 'Order type not found');

    select ttt.id
    from t_transaction_type ttt
    where ttt."name" = v_transaction_type
    into v_transaction_type_id;
    perform z_exception_if(v_transaction_type_id is null, 'Transaction type not found');

    select ta.price
    from t_task ta
    where ta.id = arg_task_id
    into v_task_price;
    perform z_exception_if(v_task_price is null, 'Failed to get task price');

    select tac.balance
    from t_account tac
    where tac.id = v_account_id
    into v_balance;
    perform z_exception_if(v_balance - v_task_price < -1000, 'Bad balance');

    insert into t_order as o (
        type_id,
        amount
    ) values (
                 v_order_type_id,
                 v_task_price
             ) returning o.id into v_order_id;
    perform z_exception_if(v_order_id is null, 'Order not created');

    insert into t_transaction as tt (
        account_id,
        date,
        type_id,
        order_id
    ) values (
                 v_account_id,
                 CURRENT_DATE,
                 v_transaction_type_id,
                 v_order_id
             );

    update t_account ta set
        balance = balance - v_task_price
    where ta.id = v_account_id
    returning ta.balance into v_balance;

    return v_balance;
end;
$$
    LANGUAGE plpgsql;

ALTER FUNCTION public.fn_transaction_task(int, int, text) OWNER TO postgres;
COMMENT ON FUNCTION public.fn_transaction_task(int, int, text) IS 'Transactions. Task.';

------------------------------------------------------------------------------------------------------------------------

CREATE TABLE t_user (
                        id SERIAL PRIMARY KEY,
                        login TEXT NOT NULL,
                        password_hash TEXT NOT NULL,
                        name TEXT NOT NULL,
                        sname TEXT NOT NULL,
                        pname TEXT,
                        role TEXT DEFAULT 'user'
);

------------------------------------------------------------------------------------------------------------------------

CREATE TABLE t_student (
                           id serial PRIMARY KEY,
                           user_id INT NOT NULL,
                           group_id INT NOT NULL
);

------------------------------------------------------------------------------------------------------------------------

CREATE TABLE t_param (
                         id serial PRIMARY KEY,
                         name TEXT NOT NULL,
                         value TEXT NOT NULL
);

INSERT INTO public.t_param (name, value)
VALUES ('maxdebt', 1000);

------------------------------------------------------------------------------------------------------------------------

CREATE TABLE t_task (
                        id serial PRIMARY KEY,
                        name TEXT NOT NULL,
                        price INT NOT NULL,
                        path TEXT,
                        programming_language_id INT NOT NULL DEFAULT 1
);

insert into t_task (
    name,
    price,
    path
) values (
    'getlost',
    200,
    './pkg/tasks/getlost.go'
);

------------------------------------------------------------------------------------------------------------------------

CREATE TABLE t_programming_language (
                                        id SERIAL PRIMARY KEY,
                                        name TEXT NOT NULL
);

------------------------------------------------------------------------------------------------------------------------

CREATE TABLE t_order_type (
                              id INT PRIMARY KEY,
                              name TEXT NOT NULL
);

INSERT INTO public.t_order_type (id,"name")
VALUES (1,'task'), (2, 'refill');

------------------------------------------------------------------------------------------------------------------------

CREATE TABLE t_order (
                         id SERIAL PRIMARY KEY,
                         type_id INT NOT NULL,
                         task_id INT,
                         amount INT NOT NULL
);

------------------------------------------------------------------------------------------------------------------------

CREATE TABLE t_account (
                           id serial PRIMARY KEY,
                           student_id INT NOT NULL,
                           balance INT DEFAULT 0
);

------------------------------------------------------------------------------------------------------------------------

CREATE TABLE t_transaction_type (
                                    id INT PRIMARY KEY,
                                    name TEXT
);

INSERT INTO public.t_transaction_type (id,"name")
VALUES (1,'negative'), (2, 'positive');

------------------------------------------------------------------------------------------------------------------------

CREATE TABLE t_transaction (
                               id serial PRIMARY KEY,
                               account_id INT NOT NULL,
                               date TIMESTAMP WITHOUT TIME ZONE,
                               type_id INT NOT NULL,
                               order_id INT NOT NULL
);

------------------------------------------------------------------------------------------------------------------------

CREATE TABLE t_university_group (
                                    id serial PRIMARY KEY,
                                    name TEXT NOT NULL
);

------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE VIEW public.v_user AS
SELECT
    u.id,
    u.name || u.sname || u.pname as fullname
FROM t_user u
ORDER BY u.id
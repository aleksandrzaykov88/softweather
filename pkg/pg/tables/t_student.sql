CREATE TABLE t_student (
    id serial PRIMARY KEY,
    user_id INT NOT NULL,
    group_id INT NOT NULL
);
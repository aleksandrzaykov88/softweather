CREATE TABLE t_param (
    id serial PRIMARY KEY,
    name TEXT NOT NULL,
    value TEXT NOT NULL
);

INSERT INTO public.t_param (name, value)
VALUES ('maxdebt', 1000)
CREATE TABLE t_order_type (
    id INT PRIMARY KEY,
    name TEXT NOT NULL
);

INSERT INTO public.t_order_type (id,"name")
VALUES (1,'task'), (2, 'refill')
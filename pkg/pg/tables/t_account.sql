CREATE TABLE t_account (
    id serial PRIMARY KEY,
    student_id INT NOT NULL,
    balance INT DEFAULT 0
);
CREATE TABLE t_user (
    id SERIAL PRIMARY KEY,
    login TEXT NOT NULL,
    password_hash TEXT NOT NULL,
    name TEXT NOT NULL,
    sname TEXT NOT NULL,
    pname TEXT,
    role TEXT DEFAULT 'user'
);
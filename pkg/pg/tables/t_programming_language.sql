CREATE TABLE t_programming_language (
    id SERIAL PRIMARY KEY,
    name TEXT NOT NULL
);
CREATE TABLE t_transaction_type (
    id INT PRIMARY KEY,
    name TEXT
);

INSERT INTO public.t_transaction_type (id,"name")
VALUES (1,'negative'), (2, 'positive')
CREATE TABLE t_task (
    id serial PRIMARY KEY,
    name TEXT NOT NULL,
    price INT NOT NULL,
    path TEXT,
    programming_language_id INT NOT NULL DEFAULT 1
);
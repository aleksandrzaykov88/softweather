CREATE TABLE t_transaction (
    id serial PRIMARY KEY,
    account_id INT NOT NULL,
    date TIMESTAMP WITHOUT TIME ZONE,
    type_id INT NOT NULL,
    order_id INT NOT NULL
);


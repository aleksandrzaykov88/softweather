CREATE TABLE t_order (
    id SERIAL PRIMARY KEY,
    type_id INT NOT NULL,
    task_id INT,
    amount INT NOT NULL
);
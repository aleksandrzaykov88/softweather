CREATE OR REPLACE VIEW public.v_user AS
    SELECT
        u.id,
        u.name || u.sname || u.pname as fullname
    FROM t_user u
    ORDER BY u.id
-- Function: public.fn_transaction_task(int, int, text)

-- DROP FUNCTION public.fn_transaction_task(int, int, text);
CREATE OR REPLACE FUNCTION public.fn_transaction_task(
    arg_user_id int,
    arg_task_id int,
    arg_user_role text
)
    RETURNS INT AS
$$
declare
    v_balance int;
    v_task_price int;
    v_account_id int;
    v_order_id int;
    v_order_type_id int;
    v_transaction_type_id int;
    v_transaction_type text = 'negative';
    v_order_type text = 'task';
begin
    perform z_exception_if(arg_user_id is null, 'User id is empty');
    perform z_exception_if(arg_task_id is null, 'Task id is empty');
    perform z_exception_if(arg_user_role is null or arg_user_role = '' or arg_user_role = 'user', 'Incorrect role');

    select ta.id
    from t_account ta
             join t_student ts on ta.student_id = ts.id
             join t_user tu on ts.user_id = tu.id
    where tu.id = arg_user_id
    into v_account_id;
    perform z_exception_if(v_account_id is null, 'Account id not found');

    select tot.id
    from t_order_type tot
    where tot."name" = v_order_type
    into v_order_type_id;
    perform z_exception_if(v_order_type_id is null, 'Order type not found');

    select ttt.id
    from t_transaction_type ttt
    where ttt."name" = v_transaction_type
    into v_transaction_type_id;
    perform z_exception_if(v_transaction_type_id is null, 'Transaction type not found');

    select ta.price
    from t_task ta
    where ta.id = arg_task_id
    into v_task_price;
    perform z_exception_if(v_task_price is null, 'Failed to get task price');

    select tac.balance
    from t_account tac
    where tac.id = v_account_id
    into v_balance;
    perform z_exception_if(v_balance - v_task_price < -1000, 'Bad balance');

    insert into t_order as o (
        type_id,
        amount
    ) values (
        v_order_type_id,
        v_task_price
    ) returning o.id into v_order_id;
    perform z_exception_if(v_order_id is null, 'Order not created');

    insert into t_transaction as tt (
        account_id,
        date,
        type_id,
        order_id
    ) values (
        v_account_id,
        CURRENT_DATE,
        v_transaction_type_id,
        v_order_id
    );

    update t_account ta set
        balance = balance - v_task_price
    where ta.id = v_account_id
    returning ta.balance into v_balance;

    return v_balance;
end;
$$
    LANGUAGE plpgsql;

ALTER FUNCTION public.fn_transaction_task(int, int, text) OWNER TO postgres;
COMMENT ON FUNCTION public.fn_transaction_task(int, int, text) IS 'Transactions. Task.';

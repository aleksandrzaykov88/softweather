CREATE OR REPLACE FUNCTION z_exception_if(
    condition boolean,
    message text
)
    RETURNS VOID AS $$
BEGIN
    IF condition THEN
        RAISE EXCEPTION '%', message;
    END IF;
END;
$$ LANGUAGE plpgsql;
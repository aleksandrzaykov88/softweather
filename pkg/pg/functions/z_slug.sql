CREATE OR REPLACE FUNCTION z_slug(arg_text text)
    RETURNS text as
$BODY$
DECLARE
    output_text text;
BEGIN
    output_text := translate(arg_text,
                             'абвгдежзийклмнопрстуфхцчшщъыьэюя',
                             'abvgdegziyklmnoprstufhzcss_y_eua');
    output_text := regexp_replace(output_text, '[^a-zA-Z0-9]+', '-', 'g');
    output_text := regexp_replace(output_text, '^-|-$', '', 'g');
    output_text := lower(output_text);

    RETURN output_text;
END;
$BODY$
    LANGUAGE plpgsql;
-- Function: public.fn_user_ins(text, text, text, text, text, text)

-- DROP FUNCTION public.fn_user_ins(text, text, text, text, text, text);
CREATE OR REPLACE FUNCTION public.fn_user_ins(
    arg_name text,
    arg_sname text,
    arg_pname text,
    arg_login text,
    arg_password text,
    arg_group text
)
    RETURNS TEXT AS
$$
declare
    v_id int;
    v_group_id int;
    v_student_id int;
    v_login text;
    v_temp text;
    v_counter int = 1;
begin
    perform z_exception_if(arg_name is null or arg_name = '', 'User name is empty');
    perform z_exception_if(arg_sname is null or arg_sname = '', 'User surname is empty');
    perform z_exception_if(arg_password is null or arg_password = '', 'Password is empty');

    v_login = arg_login;
    if arg_login is null or arg_login = '' then
        v_login = z_slug(arg_name || ' ' || arg_sname);
    end if;

    v_temp = v_login;
    while exists(
        SELECT 1
        FROM t_user u
        WHERE u.login = v_login
    ) loop
        v_login = v_temp || '_' || v_counter::TEXT;
        v_counter = v_counter + 1;
    end loop;

    insert into t_user as u (
        login,
        password_hash,
        name,
        sname,
        pname
    ) values (
        v_login,
        arg_password,
        arg_name,
        arg_sname,
        arg_pname
    ) returning u.id into v_id;

    if arg_group is not null and arg_group <> '' then
        insert into t_university_group as ug (
            name
        ) values (
            arg_group
        ) returning ug.id into v_group_id;

        insert into t_student as s (
            user_id,
            group_id
        ) values (
            v_id,
            v_group_id
        ) returning s.id into v_student_id;

        insert into t_account as a (
            student_id
        ) values (
            v_student_id
        );

        update t_user set
            role = 'student'
        where id = v_id;
    end if;

    return v_login;
end;
$$
    LANGUAGE plpgsql;

ALTER FUNCTION public.fn_user_ins(text, text, text, text, text, text) OWNER TO postgres;
COMMENT ON FUNCTION public.fn_user_ins(text, text, text, text, text, text) IS 'Users. Registration';

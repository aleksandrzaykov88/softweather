package tasks

import (
	"testing"
)

func TestGetLost(t *testing.T) {
	s := []int{1, 2, 3, 4, 5, 5, 7, 8, 8, 10}
	missingNumbers := GetLost(s)
	for i := 1; i <= len(s); i++ {
		if missingNumbers[i-1] != i {
			t.Error("result is incorrect")
		}
	}
}

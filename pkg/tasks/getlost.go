package tasks

import (
	"sort"
)

// GetLost finds all missing nums in unsorted slice.
func GetLost(nums []int) []int {
	missingNums := make(map[int]bool)
	duplicates := make(map[int]bool)

	for _, num := range nums {
		if duplicates[num] {
			continue
		}
		if missingNums[num] {
			delete(missingNums, num)
			duplicates[num] = true
		} else {
			missingNums[num] = true
		}
	}

	for i := 1; i <= len(nums); i++ {
		if !missingNums[i] {
			missingNums[i] = true
		}
	}

	var missingNumbers []int
	for num := range missingNums {
		missingNumbers = append(missingNumbers, num)
	}

	sort.Ints(missingNumbers)

	return missingNumbers
}

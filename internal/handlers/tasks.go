package handlers

import (
	"gitlab.com/aleksandrzaykov88/softweather/internal/repositories"
	"gitlab.com/aleksandrzaykov88/softweather/restapi/operations/tasks"
)

// GetTask returns file with task code.
func GetTask(repo repositories.Tasks, params tasks.GetTaskParams, userID int, userRole string) (int, string, error) {
	status, payload, err := repo.GetTask(params.HTTPRequest, params.ID, userID, userRole)
	return status, payload, err
}

package handlers

import (
	"github.com/go-openapi/runtime/middleware"
	"github.com/rs/zerolog/log"
	"gitlab.com/aleksandrzaykov88/softweather/internal/repositories"
	"gitlab.com/aleksandrzaykov88/softweather/models"
	"gitlab.com/aleksandrzaykov88/softweather/restapi/operations/users"
)

const usersModule = "users"

// GetUsers returns list of users.
func GetUsers(repo repositories.Users, params users.GetUsersParams) middleware.Responder {
	status, payload, err := repo.GetUsers(params.HTTPRequest)
	if err != nil {
		if status == 400 {
			log.Error().Err(err).Msg(usersModule)
			return users.NewGetUsersBadRequest().WithPayload(&models.Error{Code: int64(status), Message: err.Error()})
		}
		if status == 403 {
			log.Error().Err(err).Msg(usersModule)
			return users.NewGetUserForbidden().WithPayload(&models.Error{Code: int64(status), Message: err.Error()})
		}
		if status == 500 {
			log.Error().Err(err).Msg(usersModule)
			return users.NewGetUsersInternalServerError().WithPayload(&models.Error{Code: int64(status), Message: err.Error()})
		}
	}
	return users.NewGetUsersOK().WithPayload(payload)
}

// RegisterUser create user account.
func RegisterUser(repo repositories.Users, params users.RegistrationParams) middleware.Responder {
	status, payload, err := repo.RegisterUser(params.HTTPRequest, *params.User)
	if err != nil {
		if status == 400 {
			log.Error().Err(err).Msg(usersModule)
			return users.NewRegistrationBadRequest().WithPayload(&models.Error{Code: int64(status), Message: err.Error()})
		}
		if status == 500 {
			log.Error().Err(err).Msg(usersModule)
			return users.NewRegistrationInternalServerError().WithPayload(&models.Error{Code: int64(status), Message: err.Error()})
		}
	}
	return users.NewRegistrationOK().WithPayload(payload)
}

// Login authenticates user.
func Login(repo repositories.Users, params users.LoginParams) middleware.Responder {
	status, payload, err := repo.Login(params.HTTPRequest, *params.User)
	if err != nil {
		if status == 400 {
			log.Error().Err(err).Msg(usersModule)
			return users.NewLoginBadRequest().WithPayload(&models.Error{Code: int64(status), Message: err.Error()})
		}
		if status == 403 {
			log.Error().Err(err).Msg(usersModule)
			return users.NewLoginForbidden().WithPayload(&models.Error{Code: int64(status), Message: err.Error()})
		}
		if status == 500 {
			log.Error().Err(err).Msg(usersModule)
			return users.NewLoginInternalServerError().WithPayload(&models.Error{Code: int64(status), Message: err.Error()})
		}
	}
	return users.NewLoginOK().WithPayload(payload)
}

package handlers

import (
	"github.com/go-openapi/runtime/middleware"
	"github.com/rs/zerolog/log"
	"gitlab.com/aleksandrzaykov88/softweather/internal/repositories"
	"gitlab.com/aleksandrzaykov88/softweather/models"
	"gitlab.com/aleksandrzaykov88/softweather/restapi/operations/transactions"
)

const transactionsModule = "transactions"

// Refill top up student balance.
func Refill(repo repositories.Transactions, params transactions.RefillParams, userID int, userRole string) middleware.Responder {
	status, payload, err := repo.Refill(params.HTTPRequest, *params.Payment, userID, userRole)
	if err != nil {
		if status == 400 {
			log.Error().Err(err).Msg(transactionsModule)
			return transactions.NewRefillBadRequest().WithPayload(&models.Error{Code: int64(status), Message: err.Error()})
		}
		if status == 403 {
			log.Error().Err(err).Msg(transactionsModule)
			return transactions.NewRefillForbidden().WithPayload(&models.Error{Code: int64(status), Message: err.Error()})
		}
		if status == 500 {
			log.Error().Err(err).Msg(transactionsModule)
			return transactions.NewRefillInternalServerError().WithPayload(&models.Error{Code: int64(status), Message: err.Error()})
		}
	}
	return transactions.NewRefillOK().WithPayload(payload)
}

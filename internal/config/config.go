package config

import (
	"fmt"
	"sync"

	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
)

var (
	once sync.Once
	cfg  Config
)

// Config describes API configuration.
type Config struct {
	HTTP httpConfig
	DB   dbConfig
}

// httpConfig describes HTTP connection configuration.
type httpConfig struct {
	Host           string
	Port           string
	TrustedSources []string
}

// dbConfig describes database connection configuration.
type dbConfig struct {
	ConnectionString string
}

// Get init and return Config singleton.
func Get() *Config {
	once.Do(func() {
		initCfg()
	})
	return &cfg
}

// initCfg initialize Config.
func initCfg() {
	cfg = Config{}

	// Read Config.
	if err := viper.ReadInConfig(); err != nil {
		log.Fatal().Err(err).Msg("reading Config")
	}

	// HTTP settings.
	cfg.HTTP.Host = viper.GetString("http.host")
	cfg.HTTP.Port = viper.GetString("http.port")
	cfg.HTTP.TrustedSources = viper.GetStringSlice("http.trustedSources")

	// DB settings.
	cfg.DB.ConnectionString = fmt.Sprintf("postgresql://%v:%v@db:%v/%v?sslmode=disable",
		viper.GetString("db.user"),
		viper.GetString("db.password"),
		//viper.GetString("db.host"),
		viper.GetString("db.port"),
		viper.GetString("db.dbName"),
	)
}

package database

import (
	"database/sql"
	"errors"

	_ "github.com/lib/pq"
)

var (
	pg          *sql.DB
	errNoDbConn = errors.New("database connections does not exists")
)

// Create starts database connection.
func Create(cs string) error {
	conn, err := sql.Open("postgres", cs)
	if err != nil {
		return err
	}
	pg = conn
	return nil
}

// Get returns database connection.
func Get() (*sql.DB, error) {
	if pg == nil {
		return nil, errNoDbConn
	}
	return pg, nil
}

package tasks

import (
	"context"
	"database/sql"
	"errors"
	"net/http"
	"time"

	_ "github.com/lib/pq"
	"github.com/nullism/bqb"
)

var (
	errNoFile = errors.New("task file not found")
)

// Repository describes database connection.
type Repository struct {
	db *sql.DB
}

// New create new repository with database connection.
func New(db *sql.DB) *Repository {
	return &Repository{
		db: db,
	}
}

// GetTask gets task info from database and return file with task code.
func (repo *Repository) GetTask(r *http.Request, id int64, userID int, userRole string) (int, string, error) {
	qt := bqb.New("select * from fn_transaction_task(?,?,?)",
		userID,
		id,
		userRole,
	)
	queryTransaction, paramsTransaction, err := qt.ToPgsql()
	if err != nil {
		return http.StatusBadRequest, "", err
	}

	q := bqb.New("select path from t_task where id = ?", id)
	queryTask, paramsTask, err := q.ToPgsql()
	if err != nil {
		return http.StatusBadRequest, "", err
	}

	// Create context witch kill query if it outs from 10 seconds.
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// Starting db transaction.
	tx, err := repo.db.Begin()
	if err != nil {
		return http.StatusInternalServerError, "", err
	}
	defer tx.Rollback()

	if _, err := tx.ExecContext(ctx, queryTransaction, paramsTransaction...); err != nil {
		return http.StatusInternalServerError, "", err
	}

	var taskPath string
	if err := tx.QueryRowContext(ctx, queryTask, paramsTask...).Scan(&taskPath); err != nil {
		return http.StatusInternalServerError, "", err
	}
	if taskPath == "" {
		return http.StatusInternalServerError, "", errNoFile
	}
	err = tx.Commit()
	if err != nil {
		return http.StatusInternalServerError, "", err
	}

	return http.StatusOK, taskPath, nil
}

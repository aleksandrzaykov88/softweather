package repositories

import (
	"gitlab.com/aleksandrzaykov88/softweather/models"
	"net/http"
)

// Transactions describes methods of Transaction entity.
type Transactions interface {
	Refill(r *http.Request, payment models.Payment, userID int, userRole string) (int, *models.AccountInfo, error)
}

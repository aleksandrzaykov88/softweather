package transactions

import (
	"database/sql"
	"github.com/nullism/bqb"
	"gitlab.com/aleksandrzaykov88/softweather/models"
	"golang.org/x/net/context"
	"net/http"
	"time"
)

// Repository describes database connection.
type Repository struct {
	db *sql.DB
}

// New create new repository with database connection.
func New(db *sql.DB) *Repository {
	return &Repository{
		db: db,
	}
}

// Refill refills user account balance.
func (repo *Repository) Refill(r *http.Request, payment models.Payment, userID int, userRole string) (int, *models.AccountInfo, error) {
	q := bqb.New("select * from fn_transaction_refill(?,?,?)",
		userID,
		payment.Amount,
		userRole,
	)
	queryString, params, err := q.ToPgsql()
	if err != nil {
		return http.StatusInternalServerError, nil, err
	}

	// Create context witch kill query if it outs from 10 seconds.
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// Starting db transaction.
	tx, err := repo.db.Begin()
	if err != nil {
		return http.StatusInternalServerError, nil, err
	}
	defer tx.Rollback()

	var balance int
	if err := tx.QueryRowContext(ctx, queryString, params...).Scan(&balance); err != nil {
		return http.StatusInternalServerError, nil, err
	}
	err = tx.Commit()
	if err != nil {
		return http.StatusInternalServerError, nil, err
	}

	return http.StatusOK, &models.AccountInfo{Balance: int64(balance)}, nil
}

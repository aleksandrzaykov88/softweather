package repositories

import (
	"net/http"
)

// Tasks describes methods of Task entity.
type Tasks interface {
	GetTask(r *http.Request, id int64, userID int, userRole string) (int, string, error)
}

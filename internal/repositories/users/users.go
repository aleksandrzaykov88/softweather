package users

import (
	"context"
	"crypto/sha512"
	"database/sql"
	"errors"
	"fmt"
	"net/http"
	"time"

	"github.com/golang-jwt/jwt"
	_ "github.com/lib/pq"
	"github.com/nullism/bqb"
	"gitlab.com/aleksandrzaykov88/softweather/models"
)

const jwtKey = "g234ty2g2fsf23135546576//*``@"

type tokenClaims struct {
	jwt.StandardClaims
	UserID   int    `json:"id"`
	UserRole string `json:"role"`
}

var (
	errNoPwd     = errors.New("password is missing")
	errForbidden = errors.New("login failed")
	errSignIn    = errors.New("invalid token")
)

// Repository describes database connection.
type Repository struct {
	db *sql.DB
}

// New create new repository with database connection.
func New(db *sql.DB) *Repository {
	return &Repository{
		db: db,
	}
}

// GetUsers fetch list of users from database.
func (repo *Repository) GetUsers(r *http.Request) (int, []*models.UserOut, error) {
	q := bqb.New("select id, fullname from v_user")
	queryString, _, err := q.ToPgsql()
	if err != nil {
		return http.StatusInternalServerError, nil, err
	}

	// Create context witch kill query if it outs from 10 seconds.
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	rows, err := repo.db.QueryContext(ctx, queryString)
	if err != nil {
		return http.StatusInternalServerError, nil, err
	}
	defer rows.Close()

	var users []*models.UserOut
	for rows.Next() {
		var u models.UserOut
		if err := rows.Scan(&u.ID, &u.FullName); err != nil {
			return http.StatusInternalServerError, nil, err
		}
		users = append(users, &u)
	}

	return http.StatusOK, users, nil
}

// RegisterUser create user account in database.
// If user is student, RegisterUser also create student account.
// Returns user login string.
func (repo *Repository) RegisterUser(r *http.Request, body models.UserIn) (int, *models.Login, error) {
	if body.Password == nil {
		return http.StatusBadRequest, nil, errNoPwd
	}
	hasher := sha512.New()
	if _, err := hasher.Write([]byte(*body.Password)); err != nil {
		return http.StatusBadRequest, nil, err
	}

	q := bqb.New("select * from fn_user_ins(?,?,?,?,?,?)",
		body.Name,
		body.SName,
		body.PName,
		body.Login,
		fmt.Sprintf("%x", hasher.Sum(nil)),
		body.Student.GroupName,
	)
	queryString, params, err := q.ToPgsql()
	if err != nil {
		return http.StatusInternalServerError, nil, err
	}

	// Create context witch kill query if it outs from 10 seconds.
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var login string
	if err := repo.db.QueryRowContext(ctx, queryString, params...).Scan(&login); err != nil {
		return http.StatusInternalServerError, nil, err
	}

	return http.StatusOK, &models.Login{Login: login}, nil
}

// Login authenticates user by login and password.
// Returns JWT-token.
func (repo *Repository) Login(r *http.Request, body models.AuthData) (int, *models.Token, error) {
	if body.Password == nil {
		return http.StatusBadRequest, nil, errNoPwd
	}
	hasher := sha512.New()
	if _, err := hasher.Write([]byte(*body.Password)); err != nil {
		return http.StatusBadRequest, nil, err
	}

	q := bqb.New("select u.id, u.role from t_user u where u.password_hash = ? and u.login = ?",
		fmt.Sprintf("%x", hasher.Sum(nil)),
		body.Login,
	)
	queryString, params, err := q.ToPgsql()
	if err != nil {
		return http.StatusInternalServerError, nil, err
	}

	// Create context witch kill query if it outs from 10 seconds.
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var (
		userID int
		role   string
	)
	if err := repo.db.QueryRowContext(ctx, queryString, params...).Scan(&userID, &role); err != nil {
		return http.StatusInternalServerError, nil, err
	}
	if role == "" || userID == 0 {
		return http.StatusForbidden, nil, errForbidden
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, &tokenClaims{
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(12 * time.Hour).Unix(),
			IssuedAt:  time.Now().Unix(),
		},
		userID,
		role,
	})

	jwtString, err := token.SignedString([]byte(jwtKey))
	if err != nil {
		return http.StatusInternalServerError, nil, err
	}

	return http.StatusOK, &models.Token{Token: jwtString}, nil
}

// IsJWTValid validates X-Token.
// It also returns token claims.
func IsJWTValid(jwtoken string) (int, string, error) {
	token, err := jwt.ParseWithClaims(jwtoken, &tokenClaims{}, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, errSignIn
		}
		return []byte(jwtKey), nil
	})
	if err != nil {
		return 0, "", err
	}
	claims, ok := token.Claims.(*tokenClaims)
	if !ok || !token.Valid {
		return 0, "", errors.New("token is not valid")
	}

	return claims.UserID, claims.UserRole, nil
}

package repositories

import (
	"net/http"

	"gitlab.com/aleksandrzaykov88/softweather/models"
)

// Users describes methods of User entity.
type Users interface {
	GetUsers(r *http.Request) (int, []*models.UserOut, error)
	RegisterUser(r *http.Request, body models.UserIn) (int, *models.Login, error)
	Login(r *http.Request, authData models.AuthData) (int, *models.Token, error)
}

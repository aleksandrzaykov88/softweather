start:
	docker compose up -d
migrate:
	migrate -source file://./pkg/pg/migrations -database "postgres://localhost:5432/softweather?user=postgres&password=postgres&sslmode=disable" up

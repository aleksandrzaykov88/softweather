SoftWeatherAPI
==============

Golang test task for SoftWeather company.

## Deploy

First of all, we need to get migration utility from `https://github.com/golang-migrate/migrate/releases`

After that:

- make start
- make migrate

## Before beginning

### Registration
Before doing business requests to API, you should Register new user by sending request on `/api/v1/user`.

Request body structure:

```
    {
        "name": "Username",
        "sName": "Surname",
        "pName": "Patronymic",
        "password": "*******",
        "student": {
            "groupName": "GROUP-123"
        }
}
```

Response:

```
{
    "login": "userlogin"
}
```

The response will return a new login.

### Authentication
After registration, you should go to *Login* route `/api/v1/login`:

Request body structure:

```
{
    "login": "login",
    "password": "*******"
}
```
Response:

```
{
    "token": "eyJhbGciOiJIUzI1NilhdCI6MTY4ODI5NIjoic3R1ZGVudCJ9.G-mfVa51gFM8nz7WtepvERSEjzClUMQidEzmRNhV1nQ"
}
```

The response will return authentication token, which you should add to all next requests to API as `X-Token` header.


## Task

### First Step

Text:

`Создание нового студента, используя его ФИО и номер группы, например, Штирлиц Иван Васильевич ИП-394. 
Для удобства дальнейших запросов, каждому пользователю можно присваивать логин, 
который будет в роли ярлыка для этого студента.`

This step has been already described in `Before Beginning` block

### Second Step

Text:

`
Изменение долга студента (уменьшение и увеличение). Максимальный долг 1000 рублей. При достижении максимального долга, 
приложение не должно давать ответы на новые запросы решения алгоритмических задач.
`

To increase the student's debt you should to `Third Step`.

To decrease the student's debt you should call to *Refill* method:  `/api/v1/refill`
(`Reducing a student's debt (and increasing balance on his account) can go on for eternity +_+`)

Request body structure:

```
    {
        "amount": 325
    }
```

Response:

```
    {
        "balance": 325
    }
```

*balance* field in json-response display the current user balance after top up.

### Third Step

Text:

`
Получение ответа на алгоритмическую задачу. У каждой задачи есть своя стоимость для получения ответа, 
поэтому необходимо изменять долг студента, для которого надо получить ответ (не забудь про максимальный долг).
`

When authenticated user go to `api/v1/task/:id` route, he initiates query for get answer on some *Task* (chosen by id). 
In this demo version there is just one Task with ID = 1. The cost of the task will be deducted from the user's account.

Request structure:

```
    Only *id* as path-param.
```

If there are enough balance, user get `*.go` file with Task answer.

Response:

```
    File
```

If the user's debt more than 1000, user get such response:

Response:

```
    pq: Bad balance
```

## And here we are!

# Fin.
